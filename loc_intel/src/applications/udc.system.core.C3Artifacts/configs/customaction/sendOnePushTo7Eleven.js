function isValid(datapoint) {
    if (datapoint === undefined || datapoint == "null" || datapoint == null || !datapoint || String(datapoint) === 'undefined') {
        return false;
    } else {
        return true;
    }
}

try {
  
  	var isDebug = true;
  
    var base_url = "https://api.7-eleven.com/";
    //var base_url = "https://api-stage.7-eleven.com/";
    var auth_url = base_url + "auth/token";
  	//
  	// STAGING CREDS for 7-Eleven
  	//
    //var auth_payload_json = {
    //    "client_id": "XESjAD77EaetUukf31uKUgQnhi3H8nqomqL6jIC6",
    //    "client_secret": "C2tGhOAqaQJ56maGaJ62mu0FsNlIig4fjtb8QNUyUEOtUAholnIvgTvUMO63bT8i7BhesbgOcF52Oao9adAFGvkZhDksJVvi8YJP0n9S8t5nh61mhq20HrzuzR6113C6",
    //    "grant_type": "client_credentials"
    //};
  	//
  	// PRODUCTION CREDS for 7-Eleven
  	//  
    var auth_payload_json = {
        "client_id": "isRRnKH5wzwY9FFPOWQmTW6MPtkmLBV8nz0xGt2x",
        "client_secret": "VgXvrw0wsFppZ31BdodcfJOJT9J7JLHSxPd2C60AL75YLKsR6xGZrC0HECW41lo9lvJR2qG86vaBywq3GaceFh84mJvu3DkpPpkE4OAet4qMSzz4Zyq8eHbPMOtahkFd",
        "grant_type": "client_credentials"
    };
    var access_token_field = "access_token";
    var access_token_ttl_field = "expires_in";
    var auth_type = "bearer";
    var rest_url = base_url + "v4/batch/jobs/";
  
  	// Get offer data
  	var offerTitle = String(Context.get("offer_title"));
  	var offerCopy = String(Context.get("offer_copy"));  
  	var audience = [];
  	audience[0] = String(Context.get("id"));
    var rest_payload = {
        "types": ["push_notification"],
        "config": {
            "loyalty_ids": audience,
            "push_notification": {
                "title": offerTitle,
                "body": offerCopy,
                "action": {
                    "type": "overlay",
                    "id": "z1_deals"
                }
            }
        }
    };
//                    "type": "deeplink",
//                    "id": "deals"
  
    var content_type = "application/json";
    var forceLog = true;

    var response = System.callDataService("sendBatchPush", {
        "auth_url": auth_url,
        "auth_payload_json": auth_payload_json,
        "access_token_field": access_token_field,
        "auth_type": auth_type,
        "access_token_ttl_field": access_token_ttl_field,
        "rest_url": rest_url,
        "rest_payload": rest_payload,
        "content_type": content_type,
        "forceLog": true
    });

    if (isDebug) System.print(response);
  
} catch (ie) {
    System.print("[Custom Action sendOnePushTo7Eleven] exception in cus action = " + ie);
}

/*
{
	"types": ["push_notification"],
	"config": {
		"loyalty_ids": ["1785773300087144634","1758773300003803906","1758773300021831954","1758773300180699887"],
		"push_notification": {
		"title": "Z1-04 - Free Slurpee Friday is here!",
        "body": "Z1TEST - Come into 7-11 today and scan your app to get a free slurpee.",
		"action": {
			"type": "deeplink",
			"id": "deals"
			}
		}
	}
}
*/

