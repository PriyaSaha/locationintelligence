try {
    var inDataStr = Channel.getData();
    java.lang.System.out.println("udData in = " + inDataStr);

    var obj = JSON.parse(inDataStr);

    var PromoID = obj.PromoID + "";
    var projectid = obj.events[0].Projectid + "";
    var ProjectTitle = obj.events[0].ProjectTitle;
    var StartDate = obj.events[0].StartDate;
    var EndDate = obj.events[0].EndDate;
    var OfferCopy = obj.events[0].OfferCopy;
    var Group1 = obj.events[0].Group1;
    var Group2 = obj.events[0].Group2;
    var Group3 = obj.events[0].Group3;
    var Geography = obj.events[0].Geography;
    var Vendor = obj.events[0].Vendor;
    var SlinPSA = obj.events[0].SlinPSA + "";

    var udData = {

        "PromoID": PromoID,
        "Projectid": projectid,
        "ProjectTitle": ProjectTitle,
        "StartDate": StartDate,
        "EndDate": EndDate,
        "OfferCopy": OfferCopy,
        "Group1": Group1,
        "Group2": Group2,
        "Group3": Group3,
        "Geography": Geography,
        "Vendor": Vendor,
        "SlinPSA": SlinPSA
        //"PsaName": psaname,
        //"PSACategoryName": PSACategoryName,
        //"PSASubCategoryName": PSASubCategoryName
    };


    /* Update entity to log traffic */
    udData._rowkeysuffix = String(udData.PromoID);
    var _rowkey = Entity.add("Offer_Bank", udData);

} catch (ie) {
    java.lang.System.out.println("Exception in Offerbank_AzureBlob Webhook processing: " + ie);
}