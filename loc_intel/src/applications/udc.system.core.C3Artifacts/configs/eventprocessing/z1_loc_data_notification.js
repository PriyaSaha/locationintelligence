//return; // TODO...for ping failures March 25 7am

/*
 * This event processing code is part of Location Intelligence Module. 
 *
 * Computes the best_location and best_zip attributes and updated user
 * profile.  The attribute best_zip is also used as an index key so it
 * can be used for efficient Entity queries.
 *
 * Best location is computed based on the type of location 
 * data coming in this event (fine or coarse)
 */



var objErr = {};
objErr.src = "EP:z1_loc_data_notification";
var id = Context.get("id") + "";
//
// TODO: TESTING FOR ONE PROFILE ONLY...
//
//if (id != "1785773300087144634") return;  
var profileId = System.getProfileId("id", id) + "";

if (profileId == "a520b32c-2dd8-4827-ac59-33c68ad8f57d") {
  Context.put("best_zip", "95131");
  var z1_loc = Context.get("z1_loc"); 
  
  //county=CA|city=San Jose|country=United States|streetName=I-880 N|postalCode=95131|longitude=-121.9074748|neighborhood=North San Jose|latitude=37.3964851|accuracy=2191|type=coarse|state=CA|timestamp=20190327223314
  var loc = {"latitude":"37.3964851","longitude":"-121.9074748","ts":"20190327223314","accuracy":"24.003625","zip":"95131","type":"coarse"};
  Context.put("best_location", JSON.stringify(loc));  
  System.print("z1_loc="+z1_loc);
  return;
}

objErr.profileId = profileId;

try {

    var isDebug = false;

    function isValid(datapoint) {
        if (!datapoint || typeof datapoint == 'undefined' || datapoint == undefined || datapoint === undefined || String(datapoint) === 'undefined' || datapoint == "null") {
            return false;
        } else {
            return true;
        }
    }
  
    // Update Entity Raw_Locations which stores last 50 locations 
    // for this customer
    var resp0 = System.callDataService("updateRawLocations", {}) + "";
    System.print("DS updateRawLocations response = " + resp0);
    
    if (resp0 != "success") {
        objErr.p1 = "Call to DS:updateRawLocations returned with failure";
        var ts = (new Date()).getTime() + "";
        objErr.ts = ts;
        objErr._rowkeysuffix = String(profileId + ts);
        var _rowkey = Entity.add("Logs_Error", objErr);
        return;
    }
  
    // Compute attribute best_location
    var resp = System.callDataService("getBestLocation", {}) + "";
  	if (isValid(resp)) {
  	    var obj = JSON.parse(resp);
  	    if (obj) {
  	        if (isDebug) System.print("[z1_loc_data_notification] best_location zip = " + obj.zip);

  	        // TODO: May need to change the indexed attr to best_location
  	        // Since we are using attribute "best_zip" as indexed, we need to
  	        // ensure both profile and relation tables are updated with 
  	        // zip in the incoming location update
  	        Context.put("best_zip", obj.zip); // TODO Check if not getting assigned always???
  	    }
    }
} catch (ie) {

    objErr.msg = ie;

    var ts = (new Date()).getTime() + "";
    objErr.ts = ts;
    objErr._rowkeysuffix = String(profileId + ts);
    var _rowkey = Entity.add("Logs_Error", objErr);

    System.print("[z1_loc_data_notification] Exception in evt proc..." + ie);
}
