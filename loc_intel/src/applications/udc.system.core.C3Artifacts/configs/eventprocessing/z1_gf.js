try {

  	var isDebug = true;
  
    var lat = Event.get('lat');
    var long = Event.get('long');

    if (!lat || !long) return;

    if (isDebug) System.print("lat=" + lat + " long=" + long);
    var lat2 = Number(lat).toFixed(3) + "";
    var lon2 = Number(long).toFixed(3) + "";
    var entity = Entity.findByRowkeySuffix("LatLon_to_Store", lat2 + lon2);
  	if (isDebug) System.print("Found Entity containing LatLon to Store match 3dig="+entity);
  
    if (entity) {
        var useCase = entity.useCase + "";
      	if (isDebug) System.print("GF is setup only for use case = "+useCase);
        if (useCase == "6") {
            // Beverage Bar
            var data = {
                storeId: entity.storeId + "",
                zip: entity.zip + "",
                storeName: entity.storeName + ""
            };
            if (isDebug) System.print("UC6: Firing getbeveragebardeals evt with data=" + data);
            Context.fireEvent("getbeveragebardeals", data);

        } else if (useCase == "5") {
            // Store with no pump
            var data = {
                storeId: entity.storeId + "",
                zip: entity.zip + "",
                storeName: entity.storeName + ""
            };
            if (isDebug) System.print("UC5: Firing getstorewithnopumpdeals evt with data=" + data);
            Context.fireEvent("getstorewithnopumpdeals", data);

        } else if (useCase == "4") {
            // Store with no pump
            var data = {
                storeId: entity.storeId + "",
                zip: entity.zip + "",
                storeName: entity.storeName + ""
            };
            if (isDebug) System.print("UC5: Firing getstorewithpumpdeals evt with data=" + data);
            Context.fireEvent("getstorewithpumpdeals", data);

        }
    }

} catch (ie) {
    System.print("[EP: Geofence] exception=" + ie);
}