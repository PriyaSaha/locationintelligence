try {
    //If Devide_id is not part of Percent List then Stop
  	var did = Context.get("device_id") + "";
  	var goLivePercent = 0;
    var traffic = System.callDataService("throttleTriggerTraffic", {
        "deviceId": did,
        "goLivePercent": goLivePercent
    }) + "";
    if (traffic == "false") {
        if (isDebug) System.print("Debug:Device ID ==>" + did + " Go Live Percent ==>" + goLivePercent + " Evaluation ==>" + traffic);
        return;
    }

    //
    // Send chart data to demo.zineone.com geekday account
    // EventName=geofence, Event Attr: storeNumber, customerId, storeName, time, totalKCAvail, lat, long, latlong
    //  "storeName": stName,
    //  "storeNumber": stNum,
    //

    var loyaltyPts = 100;
    var num = 100 * Number((Math.round(Math.random() * 100 * 100) / 1000).toFixed(1));
    var ptsStr = loyaltyPts + Math.round(num) + "";

    var context = {
        "lat": Event.get("lat"),
        "long": Event.get("long"),
        "id": Context.get("id"),
        "totalKCAvailNow": ptsStr
    };
    System.callCustomAction("SendGFChartDataToDemoZ1COM", context);
    return;
} catch (ie) {
    System.print("[updatedlocation] Exception = " + ie);
}

return;

/*
 * This event is from 7Rewards App 
 *
 * Computes the best_location and best_zip attributes and updated user
 * profile.  The attribute best_zip is also used as an index key so it
 * can be used for efficient Entity queries.
 *
 * Best location is computed based on the type of location 
 * data coming in this event (fine or coarse)
 */


var objErr = {};
objErr.src = "EP:updatedloc";
var id = Context.get("id") + "";
//
// TODO: TESTING FOR ONE PROFILE ONLY...
//
//if (id != "1785773300087144634") return;  
var profileId = System.getProfileId("id", id);
objErr.profileId = profileId;


function isValid(datapoint) {
    if (datapoint === undefined || datapoint == "null" || datapoint == null || !datapoint || String(datapoint) === 'undefined') {
        return false;
    } else {
        return true;
    }
}

// Trace utils
var isTrace = true;

var beginTime = new Date().getTime();

var sTime;
var startTrace = function() {
    if (!isTrace) return;
    sTime = new Date().getTime();
};

var endTrace = function(str) {
    if (!isTrace) return;
    str = str || "";
    var endTime = new Date().getTime() + "";
    System.print("Debug: [Time: " + String((endTime - sTime)) + "] " + str);
    objErr.p5 = String(endTime - sTime);
    objErr.ts = endTime;
    objErr._rowkeysuffix = String(profileId + String(endTime));
    var _rowkey = Entity.add("Logs_Error", objErr);
};

var perfTime;

try {

    var isDebug = false;
   
    var lat = Event.get("lat") + "";
    var long = Event.get("long") + "";
  
  	if (!isValid(lat)) return;
  	if (!isValid(long)) return;

	objErr.p1 = "lat"+lat+" lon"+long;
  
  	var zip;
    if (isDebug) System.print("calling getZipFromGeocode with lat=" + lat + " and lon=" + long);
  
	if (isTrace) startTrace();
    var strResp = System.callDataService("getZipFromGeocode", {
        lat: lat,
        lon: long
    }) + "";  // **** This was ESSENTIAL to make sure DataService returned value (which was default Object is cast to String Constant) ****  
  	//
  	objErr.p3 = strResp;
  	objErr.p4 = "getZipFromGeocode";
  	if (isTrace) endTrace(" Call to getZipFromGeocode testds");
  
  	//  
  	if (strResp) {
		// if (isDebug) System.print("response zip type = " + typeof strResp);
    	if (isDebug) System.print("response zip = " + strResp);
      	zip = strResp;
    } else return;
  
	if (zip) objErr.p1 = "zip"+zip;  
  
  	if (zip) {      
  	    var now = new Date().getTime();
  	    var z1_loc = "postalCode=" + zip + "|latitude=" + lat + "|longitude=" + long + "|type=fine|timestamp=" + now + "|accuracy=1";
  	    Context.put("z1_loc", z1_loc);

  	    // Update Entity Raw_Locations which stores last 50 locations 
  	    // for this customer
		if (isTrace) startTrace();      
      
  	    var resp0 = System.callDataService("updateRawLocations", {}) + "";
  	    System.print("DS updateRawLocations response = " + resp0);

  	    objErr.p3 = strResp;
  	    objErr.p4 = "updateRawLocations";
  	    if (isTrace) endTrace(" Call to updateRawLocations");     
      
  	    if (resp0 == "failure") {
  	        return;
  	    }

  	    // Compute attribute best_location
		if (isTrace) startTrace();      
      
  	    var resp = System.callDataService("getBestLocation", {}) + "";
      
  	    objErr.p3 = strResp;
  	    objErr.p4 = "getBestLocation";
  	    if (isTrace) endTrace(" Call to getBestLocation");     
      
  	    var obj;
  	    if (isValid(resp)) obj = JSON.parse(resp);

  	    if (obj) {
  	        if (isDebug) System.print("[z1_loc_data_notification] best_location zip = " + obj.zip);

  	        // TODO: May need to change the indexed attr to best_location
  	        // Since we are using attribute "best_zip" as indexed, we need to
  	        // ensure both profile and relation tables are updated with 
  	        // zip in the incoming location update
  	        Context.put("best_zip", obj.zip); // TODO Check if not getting assigned always???
  	    }
  	}
} catch (ie) {

    objErr.msg = ie;

    var ts = (new Date()).getTime() + "";
    objErr.ts = ts;
    objErr._rowkeysuffix = String(profileId + ts);
    var _rowkey = Entity.add("Logs_Error", objErr);

    System.print("[updatedlocation] Exception in evt proc..." + ie);
}